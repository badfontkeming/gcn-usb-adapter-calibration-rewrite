﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GCNUSBFeeder
{
    public class GCNState
    {
        public int analogX;
        public int analogY;
        public int analogXOffset;
        public int analogYOffset;

        public int cstickX;
        public int cstickY;
        public int cstickXOffset;
        public int cstickYOffset;

        public int analogL;
        public int analogR;

        public bool A;
        public bool B;
        public bool X;
        public bool Y;
        public bool Z;
        public bool R;
        public bool L;
        public bool start;

        public bool up;
        public bool left;
        public bool down;
        public bool right;

        public int timeoutCounter;
        public int controllerState;
        const int STATE_ACTIVE = 0;
        const int STATE_INACTIVE = -1;
        const int STATE_TIMEOUT = 1;
        const int STATE_NEW = 2;
        const int TIMEOUT_MAX = 10;

        public int POVstate;

        public GCNState()
        {
            timeoutCounter = 100;
            controllerState = STATE_INACTIVE;

            analogX = 127;
            analogY = 127;
            analogXOffset = 0;
            analogYOffset = 0;
            cstickX = 127;
            cstickY = 127;
            cstickXOffset = 0;
            cstickYOffset = 0;
            analogL = 0;
            analogR = 0;
            A = false;
            B = false;
            X = false;
            Y = false;
            Z = false;
            R = false;
            L = false;
            start = false;
            up = false;
            left = false;
            down = false;
            right = false;
        }
        public void clearState()
        {
            analogX = 127;
            analogY = 127;
            analogXOffset = 0;
            analogYOffset = 0;
            cstickX = 127;
            cstickY = 127;
            cstickXOffset = 0;
            cstickYOffset = 0;
            analogL = 0;
            analogR = 0;
            A = false;
            B = false;
            X = false;
            Y = false;
            Z = false;
            R = false;
            L = false;
            start = false;
            up = false;
            left = false;
            down = false;
            right = false;
        }
        public bool UpdateStatus(byte input)
        {
            //4 is the status byte for an unplugged controller.
            //6 is the status for reconnected wavebird, and contains no input data.
            //other statuses are used for different types of controllers (i.e. wavebird)
            //so we avoid hardcoding for those statuses
            if (input == 4 || input == 6)
            {
                if (controllerState == STATE_NEW || controllerState == STATE_ACTIVE)
                {
                    controllerState = STATE_TIMEOUT;
                    timeoutCounter = 0;
                    return false;
                }
                if (controllerState == STATE_INACTIVE)
                {
                    return false;
                }
                if (controllerState == STATE_TIMEOUT)
                {
                    if (timeoutCounter < TIMEOUT_MAX)
                    {
                        timeoutCounter++;
                        return false;
                    }
                    else
                    {
                        controllerState = STATE_INACTIVE;
                        clearState();
                        return false;
                    }
                }

            }
            else
            {
                if (controllerState == STATE_TIMEOUT)
                {
                    controllerState = STATE_ACTIVE;
                    timeoutCounter = 0;
                    return true;
                }
                if (controllerState == STATE_ACTIVE)
                {
                    return true;
                }
                if (controllerState == STATE_NEW)
                {
                    controllerState = STATE_ACTIVE;
                    return true;
                }
                if (controllerState == STATE_INACTIVE)
                {
                    controllerState = STATE_NEW;
                    timeoutCounter = 0;
                    return true;
                }
            }
            //shouldn't be able to get here.
            return false;
        }
        private void CalibrateController(byte[] input)
        {
            analogXOffset = 127 - (int)input[3];
            analogYOffset = 127 - (int)input[4];
            cstickXOffset = 127 - (int)input[5];
            cstickYOffset = 127 - (int)input[6];
        }

        public void UpdateState(byte[] input)
        {
            //[0] joystick enabled
            //[1] upper end D-Pad, lower end a,b,x,y
            //[2] R button, L Button, z, start
            //[3] analog X
            //[4] analog Y
            //[5] c-stick X
            //[6] c-stick Y
            //[7] L axis
            //[8] R Axis

            //[1] [0]: A
            //    [1]: B
            //    [2]: X
            //    [3]: Y
            //    [4]: Left
            //    [5]: Right
            //    [6]: Down
            //    [7]: Up

            //[2] [0]: start
            //    [1]: z
            //    [2]: R button
            //    [3]: L Button
            //    [4]: not used
            //    [5]: not used
            //    [6]: not used
            //    [7]: not used
            if (controllerState == STATE_NEW)
            {
                CalibrateController(input);
            }
            if (input.Length == 9)
            {
                if ((int)input[0] > 0)
                {
                    byte b1 = input[1];
                    A = (b1 & (1 << 0)) != 0;
                    B = (b1 & (1 << 1)) != 0;
                    X = (b1 & (1 << 2)) != 0;
                    Y = (b1 & (1 << 3)) != 0;

                    left = (b1 & (1 << 4)) != 0;
                    right = (b1 & (1 << 5)) != 0;
                    down = (b1 & (1 << 6)) != 0;
                    up = (b1 & (1 << 7)) != 0;

                    //Generate POV state for vJoy.
                    if (right) { POVstate = 1; }
                    else if (down) { POVstate = 2; }
                    else if (left) { POVstate = 3; }
                    else if (up) { POVstate = 0; }
                    else { POVstate = -1; }

                    byte b2 = input[2];
                    start = (b2 & (1 << 0)) != 0;
                    Z = (b2 & (1 << 1)) != 0;
                    R = (b2 & (1 << 2)) != 0;
                    L = (b2 & (1 << 3)) != 0;

                    analogX = (int)input[3] + analogXOffset;
                    analogY = (int)input[4] + analogYOffset;
                    cstickX = (int)input[5] + cstickXOffset;
                    cstickY = (int)input[6] + cstickYOffset;
                    analogL = (int)input[7];
                    analogR = (int)input[8];
                }
                return;
            }
            else
            {
                throw new Exception("Invalid byte array for input");
            }
        }
    }
}
